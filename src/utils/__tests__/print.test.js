const {print} = require('../add.util');

describe('print', () => {
    it('happy flow print() function', () => {
        const consolelogMocked = jest.spyOn(console, 'log');

        print(1);

        expect(consolelogMocked).toBeCalledTimes(1);
    });
});
