const addUtil = require('../add.util');

describe('add', () => {
    it('happy flow add() function', () => {
        const result = addUtil.add(1, 2);

        expect(result).toBe(3);
    });

    it('sad flow add() function', () => {
        const printMocked = jest.spyOn(addUtil, 'print')
            .mockImplementationOnce(() => {
                console.log('haaaaalooooo');
            });
        const result = addUtil.add(1, 2);

        expect(printMocked).toBeCalledTimes(0);
        expect(result).not.toBe(4);
    });

    it('happy flow add() function with a=5', () => {
        const printMocked = jest.spyOn(addUtil, 'print')
            .mockImplementationOnce(() => {
                console.log('haaaaalooooo');
            });
        const result = addUtil.add(5, 2);

        expect(printMocked).toBeCalledTimes(1);
        expect(result).toBe(7);
    });
});
