const express = require('express');
const app = express();

app.get('/', (req, res) => {
    res.json({
        foo: 'bar'
    });
});

app.listen(process.env.PORT, () => {
    console.log(`server started in port ${process.env.PORT}`);
});
